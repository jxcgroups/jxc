package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;
@Mapper
public interface SupplierDao {
    List<Supplier> list(@Param("offSet") Integer offSet,
                        @Param("rows") Integer rows,
                        @Param("goodsTypeId") String goodsTypeId);

    void save(@Param("supplier" ) Supplier supplier);

    void update(@Param("supplier" ) Supplier supplier);

    void delete(@Param("ids") Integer split);
}
