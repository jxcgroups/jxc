package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface DamageListDao {

//    @Options(useGeneratedKeys = true, keyProperty = "damageListId")
    void save(@Param("damageList") DamageList damageList);

    Integer selectDamageListId(@Param("damageNumber") String damageNumber);

    List<DamageList> list(@Param("sTime") String sTime,@Param("eTime") String eTime);
}
