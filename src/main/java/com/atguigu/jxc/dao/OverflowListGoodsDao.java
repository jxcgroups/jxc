package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.OverflowListGoods;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface OverflowListGoodsDao {
    void save(@Param("damageListGoods") OverflowListGoods damageListGoods);

    List<OverflowListGoods> goodsList(@Param("overflowListId") Integer overflowListId);
}
