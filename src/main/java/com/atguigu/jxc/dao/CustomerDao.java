package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CustomerDao {
    List<Customer> list(@Param("page") Integer page,
                        @Param("rows") Integer rows,
                        @Param("goodsTypeId") String goodsTypeId);

    void save(@Param("customer" ) Customer customer);

    void update(@Param("customer" ) Customer customer);

    void delete(@Param("ids") Integer customer);
}
