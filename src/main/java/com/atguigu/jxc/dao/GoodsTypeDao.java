package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品类别
 */
public interface GoodsTypeDao {

    List<GoodsType> getAllGoodsTypeByParentId(Integer pId);

    Integer updateGoodsTypeState(GoodsType parentGoodsType);


    void save(@Param("goodsTypeName") String goodsTypeName,@Param("pId") Integer pId);

    List<GoodsType> selectAll();



    void updateGoodsType(@Param("pId") Integer goodsTypeId);

    void delete(@Param("goodsTypeId") Integer goodsTypeId);


    Integer selectGoodsTypeId(@Param("goodsTypeId") Integer goodsTypeId);

    List<GoodsType> selectPid(@Param("pId") Integer pId);

    void update(@Param("pId") Integer pId);
}
