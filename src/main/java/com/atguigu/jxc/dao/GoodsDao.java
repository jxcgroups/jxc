package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Unit;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品信息
 */
public interface GoodsDao {


    String getMaxCode();


    List<Goods> list(@Param("page") Integer page,
                     @Param("rows") Integer rows,
                     @Param("codeOrName") String codeOrName,
                     @Param("goodsTypeId") Integer goodsTypeId);

    Integer goodNumq(@Param("goodsId") Integer goodsId);

    Integer goodNumh(@Param("goodsId") Integer goodsId);


    List<Goods> goodsList(@Param("page") Integer page,
                          @Param("rows") Integer rows,
                          @Param("goodsName") String goodsName,
                          @Param("goodsTypeId") Integer goodsTypeId);

    List<Unit> unitList();

    void save(Goods goods);

    void update(Goods goods);

    Goods selectGoodsId(@Param("goodsId") Integer goodsId);

    void delete(@Param("goodsId") Integer goodsId);

    List<Goods> notGoodsStart(@Param("offer") Integer offer,@Param("rows") Integer rows, @Param("nameOrCode") String nameOrCode);

    List<Goods> goodsStart(@Param("offer") Integer page,@Param("rows") Integer rows, @Param("nameOrCode") String nameOrCode);

    void saveStock(@Param("goodsId") Integer goodsId,
                   @Param("inventoryQuantity") Integer inventoryQuantity,
                   @Param("purchasingPrice") Double purchasingPrice);

    void deleteStock(@Param("goodsId") Integer goodsId);

    List<Goods> listAlarm();
}
