package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.CustomerService;
import com.atguigu.jxc.service.impl.CustomerServiceImpl;
import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
@Slf4j
@RestController
@RequestMapping("customer")
@EnableKnife4j
@Api(tags = "菜单控制器")
public class CustomerController {



    /**
     *客户列表查询，分页查询
     */
    @Autowired
    private CustomerService customerService;
    @PostMapping("list")
    @ApiOperation(value="查询客户列表")
    public Map<String,Object> List(Integer page, Integer rows, String customerName){
        Map<String,Object> map = customerService.List(page,rows,customerName);
        return map;
    }
    @PostMapping("save")
    public ServiceVO save(Customer customer) {
        log.info("添加操作：" + customer);
        customerService.save(customer);
        return new ServiceVO(200, "请求成功");
    }

    @PostMapping("delete")
    public ServiceVO delete(@RequestBody String  ids) {
        log.info("" + ids);
        customerService.delete(ids);
        return  new ServiceVO<>(200,"批量删除成功");
    }


}
