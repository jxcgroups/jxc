package com.atguigu.jxc.controller;

import com.atguigu.jxc.dao.UserDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.OverflowListGoodsService;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.ParameterResolutionDelegate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * 商品报溢*/
@RestController
public class OverflowListGoodsController {

@Autowired
    private OverflowListGoodsService overflowListGoodsService;

/**
 * 请求URL：http://localhost:8080/overflowListGoods/save?overflowNumber=BY1605767033015（报溢单号）
 * 请求参数：OverflowList overflowList, String overflowListGoodsStr
 * 请求方式：POST
 * 返回值类型：JSON
 * 返回值：ServiceVO
 * Response Example Value：*/
    @PostMapping("overflowListGoods/save")
    public ServiceVO save(OverflowList overflowList, String overflowListGoodsStr){
        overflowListGoodsService.save(overflowList,overflowListGoodsStr);
        return new ServiceVO(200,"报损保存成功");
    }

    /**
     * 4.3、报溢单查询
     * 请求URL：http://localhost:8080/overflowListGoods/list
     * 请求参数：String  sTime（开始时间）, String  eTime（结束时间）
     * 请求方式：POST
     * 返回值类型：JSON
     * 返回值：Map<String,Object>
     * Response Example Value：*/

    @PostMapping("overflowListGoods/list")
    public Map<String, Object> list(String sTime, String eTime) {
        Map<String, Object> map = overflowListGoodsService.list(sTime, eTime);
        return map;
    }

    /**
     * 4.4、报溢单商品信息
     * 请求URL：http://localhost:8080/overflowListGoods/goodsList
     * 请求参数：Integer overflowListId
     * 请求方式：POST
     * 返回值类型：JSON
     * 返回值：Map<String,Object>
     * Response Example Value：*/
    @PostMapping("overflowListGoods/goodsList")
    public Map<String,Object> goodsList(Integer overflowListId){
        Map<String,Object> map= overflowListGoodsService.goodsList(overflowListId);
        return map;
    }

}
