package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.service.DamageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * 商品报损
 */
@Slf4j
@RestController
@RequestMapping("/damageListGoods/")
public class DamageController {

    @Autowired
    private DamageService damageService;

    /**
     * 1.1、保存报损单
     * 请求URL：http://localhost:8080/damageListGoods/save?damageNumber=BS1605766644460（报损单号,前端生成）
     * 请求参数：DamageList damageList, String damageListGoodsStr
     * 请求方式：POST
     * 返回值类型：JSON
     * 返回值：ServiceVO
     * Response Example Value：
     */
    @PostMapping("save")
    public ServiceVO save(DamageList damageList,
                          String damageListGoodsStr) {
        log.info("" + damageList + damageListGoodsStr);
        damageService.save(damageList, damageListGoodsStr);
        return new ServiceVO(200, "保存成功");
    }

    /**
     * 4.1、报损单查询
     * 请求URL：http://localhost:8080/damageListGoods/list
     * 请求参数：String  sTime（开始时间）, String  eTime（结束时间）
     * 请求方式：POST
     * 返回值类型：JSON
     * 返回值：Map<String,Object>
     * Response Example Value：
     */
    @PostMapping("list")
    public Map<String, Object> list(String sTime, String eTime) {
        Map<String, Object> map = damageService.list(sTime, eTime);
        return map;
    }

    /**
     * 4.2、查询报损单商品信息
     * 请求URL：http://localhost:8080/damageListGoods/goodsList
     * 请求参数：Integer damageListId（报损单Id）
     * 请求方式：POST
     * 返回值类型：JSON
     * 返回值：Map<String,Object>
     * Response Example Value：*/

    @PostMapping("goodsList")
    public Map<String,Object> goodsList(Integer damageListId){
        Map<String,Object> map= damageService.goodsList(damageListId);
        return map;
    }
}
