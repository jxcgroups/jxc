package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.util.Map;


@RestController
@RequestMapping("/supplier")
@Slf4j
public class SupplierController {

    @Autowired
    private SupplierService supplierService;

    /**
     * 请求URL：http://localhost:8080/supplier/list
     * 请求参数：Integer page（当前页数）, Integer rows（每页显示的记录数）, String supplierName
     * 请求方式：POST
     * 返回值类型： JSON
     * 返回值：Map<String,Object>
     */
    @PostMapping("/list")
    public Map<String, Object> list(Integer page,
                                    Integer rows,
                                    String supplierName) {
        log.info("" + page + rows + supplierName);
        Map<String, Object> map = supplierService.list(page, rows, supplierName);
        return map;
    }

    /**
     * 请求URL：http://localhost:8080/supplier/save?supplierId=1
     * 请求参数：Supplier supplier
     * 请求方式：POST
     * 返回值类型： JSON
     * 返回值：ServiceVO
     * Response Example Value：
     */
    @PostMapping("save")
    public ServiceVO save(Supplier supplier) {
        log.info("添加操作：" + supplier);
        supplierService.save(supplier);
        return new ServiceVO(200, "请求成功");
    }

    /**
     * 请求URL：http://localhost:8080/supplier/delete
     * 请求参数：String  ids
     * 请求方式：POST
     * 返回值类型：JSON
     * 返回值：ServiceVO
     * Response Example Value：
     */
    @PostMapping("delete")
    public ServiceVO delete(@RequestBody String  ids) {
        log.info("" + ids);
        supplierService.delete(ids);
        return  new ServiceVO<>(200,"批量删除成功");
    }

}
