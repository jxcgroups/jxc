package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.service.GoodsService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description 商品信息Controller
 */
@RestController
@Slf4j
public class GoodsController {

    @Autowired
    private GoodsService goodsService;

    /**
     * 分页查询商品库存信息
     *
     * @param page        当前页
     * @param rows        每页显示条数
     * @param codeOrName  商品编码或名称
     * @param goodsTypeId 商品类别ID
     * @return
     */

    //http://localhost:8080/goods/listInventory
    @PostMapping("/goods/listInventory")
    public Map<String, Object> listInventory(Integer page,
                                             Integer rows,
                                             Integer goodsTypeId,
                                             String codeOrName

    ) {
        log.info("显示首页数据：参数：page" + page + ",rows:" + rows + ":" + codeOrName + ":" + goodsTypeId);
        Map<String, Object> map = goodsService.list(page, rows, codeOrName, goodsTypeId);
        log.info("" + map);
        return map;
    }


    /**
     * 3.2、查询所有商品单位
     * 请求URL：http://localhost:8080/unit/list
     * 请求参数：无
     * 请求方式：POST
     * 返回值类型：JSON
     * 返回值：Map<String,Object>
     * Response Example Value：
     */
    @PostMapping("unit/list")
    public Map<String, Object> unitList() {
        Map<String, Object> map = goodsService.unitList();
        return map;
    }

    /**
     * 分页查询商品信息
     *
     * @param page        当前页
     * @param rows        每页显示条数
     * @param goodsName   商品名称
     * @param goodsTypeId 商品类别ID
     * @return 请求URL：http://localhost:8080/goods/list
     * * 请求参数：Integer page, Integer rows, String goodsName, Integer goodsTypeId
     * * 请求方式：POST
     * * 返回值类型：JSON
     * * 返回值：Map<String,Object>
     * * Response Example Value：
     */
    // http://localhost:8080/goods/list
    @PostMapping("goods/list")
    public Map<String, Object> list(@RequestParam Integer page,
                                    @RequestParam Integer rows,
                                    @RequestParam(required = false) String goodsName,
                                    @RequestParam(required = false) Integer goodsTypeId) {
        log.info("" + page + ":" + rows + ":" + goodsName + ":" + goodsTypeId);
        Map<String, Object> map = goodsService.goodsList(page, rows, goodsName, goodsTypeId);

        return map;
    }


    /**
     * 生成商品编码
     *
     * @return
     */
    @RequestMapping("goods/getCode")
    @RequiresPermissions(value = "商品管理")
    public ServiceVO getCode() {
        return goodsService.getCode();
    }

    /**
     * 添加或修改商品信息
     *
     * @param goods 商品信息实体
     * @return 3.6、商品添加或修改
     * 请求URL：http://localhost:8080/goods/save?goodsId=37
     * 请求参数：Goods goods
     * 请求方式：POST
     * 返回值类型：JSON
     * 返回值：ServiceVO
     * Response Example Value：
     */
    @PostMapping("goods/save")
    public ServiceVO save(Goods goods) {
        log.info("" + goods);
        goodsService.save(goods);
        return new ServiceVO(200, "添加成功");
    }

    /**
     * 删除商品信息
     *
     * @param goodsId 商品ID
     * @return 3.7、商品删除（要判断商品状态,入库、有进货和销售单据的不能删除）
     * 请求URL：http://localhost:8080/goods/delete
     * 请求参数：Integer goodsId
     * 请求方式：POST
     * 返回值类型：JSON
     * 返回值：ServiceVO
     * Response Example Value：
     */
    @PostMapping("goods/delete")
    public ServiceVO delete(Integer goodsId) {
        log.info("删除商品数据：" + goodsId);
        goodsService.delete(goodsId);
        return new ServiceVO(200, "删除数据");
    }

    /**
     * 分页查询无库存商品信息
     *
     * @param page       当前页
     * @param rows       每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return 请求URL：http://localhost:8080/goods/getNoInventoryQuantity
     * 请求参数：Integer page,Integer rows,String nameOrCode
     * 请求方式：POST
     * 返回值类型：JSON
     * 返回值：Map<String,Object>
     * Response Example Value：
     */

    @PostMapping("/goods/getNoInventoryQuantity")
    public Map<String, Object> getNoInventoryQuantity(Integer page,
                                                      Integer rows,
                                                      String nameOrCode) {

        Map<String, Object> map = goodsService.notGoodsStart(page, rows, nameOrCode);
        return map;
    }


    /**
     * 分页查询有库存商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     * 4.2、有库存商品列表展示（可以根据商品名称或编码查询）
     * 请求URL：http://localhost:8080/goods/getHasInventoryQuantity
     * 请求参数：Integer page,Integer rows,String nameOrCode
     * 请求方式：POST
     * 返回值类型：JSON
     * 返回值：Map<String,Object>
     * Response Example Value：
     */
    @PostMapping("goods/getHasInventoryQuantity")
    public Map<String,Object> getHasInventoryQuantity(
            Integer page,
            Integer rows,
            String nameOrCode

    ){
        Map<String, Object> map = goodsService.goodsStart(page, rows, nameOrCode);
        return map;
    }


    /**
     * 添加商品期初库存
     * @param goodsId 商品ID
     * @param inventoryQuantity 库存
     * @param purchasingPrice 成本价
     * @return
     * 请求URL：http://localhost:8080/goods/saveStock?goodsId=25
     * 请求参数：Integer goodsId,Integer inventoryQuantity,double purchasingPrice
     * 请求方式：POST
     * 返回值类型：JSON
     * 返回值：ServiceVO
     * Response Example Value：
     */

    @PostMapping("goods/saveStock")
    public ServiceVO saveStock(Integer goodsId,
                               Integer inventoryQuantity,
                               Double purchasingPrice){
        log.info("+"+goodsId+inventoryQuantity+purchasingPrice);
         goodsService.saveStock(goodsId,inventoryQuantity,purchasingPrice);
        return new ServiceVO(200,"添加数据成功");
    }

    /**
     * 删除商品库存
     * @param goodsId 商品ID
     * @return
     * 4.4、删除库存（要判断商品状态 入库、有进货和销售单据的不能删除）
     * 请求URL：http://localhost:8080/goods/deleteStock
     * 请求参数：Integer goodsId
     * 请求方式：POST
     * 返回值类型：JSON
     * 返回值：ServiceVO
     * Response Example Value：
     */

    @PostMapping("goods/deleteStock")
    public ServiceVO deleteStock(Integer goodsId){
        goodsService.deleteStock(goodsId);
        return new ServiceVO(200,"删除数据成功");
    }

    /**
     * 查询库存报警商品信息
     * @return
    3.1、查询所有 当前库存量 小于 库存下限的商品信息
    请求URL：http://localhost:8080/goods/listAlarm
    请求参数：无
    请求方式：POST
    返回值类型：JSON
    返回值：Map<String,Object>
    Response Example Value：     */
    @PostMapping("goods/listAlarm")
    public Map<String,Object> listAlarm(){
       Map<String,Object> map=  goodsService.listAlarm();
        return map;
    }

}
