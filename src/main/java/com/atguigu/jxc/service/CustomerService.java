package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Supplier;

import javax.print.DocFlavor;
import java.util.Map;

public interface CustomerService {
    Map<String,Object> List(Integer page, Integer rows, String customerName);

    void save(Customer supplier);

    void delete(String ids);
}

