package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service

public class CustomerServiceImpl implements CustomerService {
    @Autowired
    private CustomerDao customerDao;
    @Override
    public Map<String, Object> List(Integer page, Integer rows, String customerName) {
        HashMap<String,Object> map = new HashMap<>();
        page =page == 0 ? 1 : page;
        int offSet = (page -1) * rows;
        List<Customer> list = customerDao.list(offSet,rows,customerName);
        map.put("total","9");
        map.put("rows",list);

        return map;
    }

    @Override
    public void save(Customer customer) {

        if (StringUtils.isEmpty(customer.getCustomerId())){
            customerDao.save(customer);
        }else {
            customerDao.update(customer);
        }
    }

    @Override
    public void delete(String ids) {
        if (StringUtils.isEmpty(ids)){
            throw new RuntimeException();
        }
        String replace = ids.replace("ids=", "");
        String[] split = replace.split("%2C");
        for (String s : split) {
            customerDao.delete(Integer.valueOf(s));
        }
    }


}
