package com.atguigu.jxc.service.impl;


import com.atguigu.jxc.dao.DamageListDao;
import com.atguigu.jxc.dao.DamageListGoodsDao;
import com.atguigu.jxc.dao.UserDao;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.DamageService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import io.github.classgraph.json.JSONUtils;
import io.swagger.annotations.Api;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import springfox.documentation.spring.web.json.Json;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.apache.logging.log4j.message.MapMessage.MapFormat.JSON;

@Service
public class DamageServiceImpl implements DamageService {

    @Autowired
    private DamageListDao damageListDao;

    @Autowired
    private DamageListGoodsDao damageListGoodsDao;
    @Autowired
    private UserDao userDao;

    private Integer userId() {
        //获取当前登录的用户名
        String userName = (String) SecurityUtils.getSubject().getPrincipal();

        //根据用户名查询用户信息
        User user = userDao.findUserByName(userName);

        //获取userId,并返回
        return user.getUserId();
    }

    @Override
    public void save(DamageList damageList, String damageListGoodsStr) {
        //1.将damageList保存到对应的数据库表中
        //获取当前userId
        damageList.setUserId(userId());

        damageListDao.save(damageList);
        Integer damageListId = damageListDao.selectDamageListId(damageList.getDamageNumber());


        //使用谷歌Gson将JSON字符串数组转换成具体的集合
        Gson gson = new Gson();
        List<DamageListGoods> damageListGoodsList =
                gson.fromJson(damageListGoodsStr, new TypeToken<List<DamageListGoods>>() {
                }.getType());
        //2.将获取到的集合添加到数据库中
        for (DamageListGoods damageListGoods : damageListGoodsList) {
            damageListGoods.setDamageListId(damageListId);
            damageListGoodsDao.save(damageListGoods);
        }

    }

    @Override
    public Map<String, Object> list(String sTime, String eTime) {
        HashMap<String, Object> map = new HashMap<>();
        List<DamageList>  damageLists=damageListDao.list(sTime,eTime);
        map.put("rows",damageLists);
        return map;
    }

    @Override
    public Map<String, Object> goodsList(Integer damageListId) {
        HashMap<String, Object> map = new HashMap<>();
        List<DamageListGoods>  damageListGoodsList=damageListGoodsDao.goodsList(damageListId);
        map.put("rows",damageListGoodsList);
        return map;
    }
}
