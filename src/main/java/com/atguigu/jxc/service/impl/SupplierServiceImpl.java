package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Service
public class SupplierServiceImpl implements SupplierService {

    @Autowired
    private SupplierDao supplierDao;
    @Override
    public Map<String, Object> list(Integer page, Integer rows, String supplierName) {
        HashMap<String, Object> map = new HashMap<>();
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        List<Supplier> suppliers=supplierDao.list(offSet,rows,supplierName);
        map.put("total","9");
        map.put("rows",suppliers);
        return map;
    }

    @Override
    public void save(Supplier supplier) {
        if (supplier.getSupplierId()==null){
            supplierDao.save(supplier);
        }else {
            supplierDao.update(supplier);
        }

    }

    @Override
    public void delete(String ids) {
        if (StringUtils.isEmpty(ids)){
            throw new RuntimeException();
        }
        String replace = ids.replace("ids=", "");
        String[] split = replace.split("%2C");
        for (String s : split) {
             System.out.println(s);
            supplierDao.delete(Integer.valueOf(s));
        }

    }
}
