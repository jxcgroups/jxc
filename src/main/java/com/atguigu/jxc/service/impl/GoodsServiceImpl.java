package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.entity.Unit;
import com.atguigu.jxc.service.CustomerReturnListGoodsService;
import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.service.LogService;
import com.atguigu.jxc.service.SaleListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;


    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for (int i = 4; i > intCode.toString().length(); i--) {

            unitCode = "0" + unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }

    @Override
    public Map<String, Object> list(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
        HashMap<String, Object> map = new HashMap<>();
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        List<Goods> goods = goodsDao.list(offSet, rows, codeOrName, goodsTypeId);
        List<Goods> goods1 = new ArrayList<>();
        for (Goods good : goods) {
            Integer goodsId = good.getGoodsId();
            Integer goodNumq = goodsDao.goodNumq(goodsId);
            Integer goodNumh = goodsDao.goodNumh(goodsId);

            if (goodNumh != null) {
                good.setSaleTotal(goodNumq - goodNumh);
            } else {
                good.setSaleTotal(goodNumq);
            }
            goods1.add(good);
        }


        map.put("total", "26");
        map.put("rows", goods1);
        return map;
    }

    @Override
    public Map<String, Object> goodsList(Integer page, Integer rows, String goodsName, Integer goodsTypeId) {
        HashMap<String, Object> map = new HashMap<>();
        List<Goods> goodsList = goodsDao.goodsList(page, rows, goodsName, goodsTypeId);
        map.put("total", "26");
        map.put("rows", goodsList);
        return map;
    }

    @Override
    public Map<String, Object> unitList() {
        HashMap<String, Object> map = new HashMap<>();

        List<Unit> list = goodsDao.unitList();
        map.put("rows", list);
        return map;
    }

    @Override
    public void save(Goods goods) {
        if (goods.getGoodsId() == null) {
            goods.setInventoryQuantity(goods.getMinNum());
            goods.setState(0);
            goodsDao.save(goods);
        } else {
            goods.setInventoryQuantity(goods.getMinNum());
            goods.setState(0);
            goodsDao.update(goods);
        }


    }

    @Override
    public void delete(Integer goodsId) {
        Goods goods = goodsDao.selectGoodsId(goodsId);
        if (goods.getState() == 0) {
            goodsDao.delete(goodsId);
        } else {
            throw new RuntimeException("删除失败");
        }

    }

    @Override
    public Map<String, Object> notGoodsStart(Integer page, Integer rows, String nameOrCode) {
        HashMap<String, Object> map = new HashMap<>();
        int offer=page-1;
        List<Goods> goodsList = goodsDao.notGoodsStart(offer, rows, nameOrCode);
        map.put("total", "8");
        map.put("rows", goodsList);
        return map;
    }

    @Override
    public Map<String, Object> goodsStart(Integer page, Integer rows, String nameOrCode) {
        HashMap<String, Object> map = new HashMap<>();
        int offer=page-1;
        List<Goods> goodsList = goodsDao.goodsStart(offer, rows, nameOrCode);
        map.put("total", "8");
        map.put("rows", goodsList);
        return map;
    }

    @Override
    public void saveStock(Integer goodsId, Integer inventoryQuantity, Double purchasingPrice) {
        goodsDao.saveStock(goodsId,inventoryQuantity,purchasingPrice);
    }

    @Override
    public void deleteStock(Integer goodsId) {
        goodsDao.deleteStock(goodsId);
        }

    @Override
    public Map<String, Object> listAlarm() {
        HashMap<String, Object> map = new HashMap<>();
        List<Goods> goodsList= goodsDao.listAlarm();
        map.put("rows",goodsList);
        return map;
    }


}
