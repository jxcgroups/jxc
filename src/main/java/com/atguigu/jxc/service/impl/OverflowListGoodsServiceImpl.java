package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.OverflowListDao;
import com.atguigu.jxc.dao.OverflowListGoodsDao;
import com.atguigu.jxc.dao.UserDao;
import com.atguigu.jxc.entity.*;
import com.atguigu.jxc.service.OverflowListGoodsService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OverflowListGoodsServiceImpl implements OverflowListGoodsService {

    @Autowired
    private OverflowListGoodsDao overflowListGoodsDao;
    @Autowired
    private OverflowListDao overflowListDao;
    @Autowired
    private UserDao userDao;

    private Integer userId() {
        //获取当前登录的用户名
        String userName = (String) SecurityUtils.getSubject().getPrincipal();

        //根据用户名查询用户信息
        User user = userDao.findUserByName(userName);

        //获取userId,并返回
        return user.getUserId();
    }

    @Override
    public void save(OverflowList damageList, String damageListGoodsStr) {
        damageList.setUserId(userId());


        overflowListDao.save(damageList);
        Integer damageListId = overflowListDao.selectDamageListId(damageList.getOverflowNumber());


        //使用谷歌Gson将JSON字符串数组转换成具体的集合
        Gson gson = new Gson();
        List<OverflowListGoods> damageListGoodsList =
                gson.fromJson(damageListGoodsStr, new TypeToken<List<OverflowListGoods>>() {
                }.getType());
        //2.将获取到的集合添加到数据库中
        for (OverflowListGoods damageListGoods : damageListGoodsList) {
            damageListGoods.setOverflowListId(damageListId);
            overflowListGoodsDao.save(damageListGoods);
        }
    }

    @Override
    public Map<String, Object> list(String sTime, String eTime) {
        HashMap<String, Object> map = new HashMap<>();
        List<OverflowList> overflowLists = overflowListDao.list(sTime, eTime);
        map.put("rows", overflowLists);
        return map;
    }

    @Override
    public Map<String, Object> goodsList(Integer overflowListId) {
        HashMap<String, Object> map = new HashMap<>();
        List<OverflowListGoods>  overflowListGoodsList=overflowListGoodsDao.goodsList(overflowListId);
        map.put("rows",overflowListGoodsList);
        return map;
    }
}
