package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Supplier;

import java.util.Map;

public interface SupplierService {
    Map<String, Object> list(Integer page, Integer rows, String supplierName);

    void save(Supplier supplier);

    void delete(String ids);
}
