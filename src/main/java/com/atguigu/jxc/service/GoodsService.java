package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;

import java.util.Map;

public interface GoodsService {


    ServiceVO getCode();


    Map<String, Object> list(Integer page, Integer rows, String codeOrName, Integer goodsTypeId);

    Map<String, Object> goodsList(Integer page, Integer rows, String goodsName, Integer goodsTypeId);

    Map<String, Object> unitList();

    void save(Goods goods);

    void delete(Integer goodsId);

    Map<String, Object> notGoodsStart(Integer page, Integer rows, String nameOrCode);

    Map<String, Object> goodsStart(Integer page, Integer rows, String nameOrCode);

    void saveStock(Integer goodsId, Integer inventoryQuantity, Double purchasingPrice);

    void deleteStock(Integer goodsId);

    Map<String, Object> listAlarm();
}
