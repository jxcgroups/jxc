package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.OverflowList;

import java.util.Map;

public interface OverflowListGoodsService {
    void save(OverflowList damageList, String damageListGoodsStr);

    Map<String, Object> list(String sTime, String eTime);

    Map<String, Object> goodsList(Integer overflowListId);
}
